#pragma once
#include "project.h"

#define PI 3.14159f

void compass__set_destination_gps(dbc_BRIDGE_GPS_s *copy_dest_data);
dbc_GEO_COMPASS_s compass__get_current_and_destination_heading(void);
void compass__read_current_gps_coordinate();

dbc_BRIDGE_GPS_s compass__get_current_coordinates(void);
dbc_BRIDGE_GPS_s compass__get_destination_coordinates(void);

float compass__calculate_destination_distance(dbc_BRIDGE_GPS_s l_current_gps_coordinates,
                                              dbc_BRIDGE_GPS_s l_destination_gps_coordinates);

bool compass__get_destination_set_flag();
