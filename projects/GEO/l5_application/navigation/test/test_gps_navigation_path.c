#include "unity.h"

#include "gps_navigation_path.h"

#include "Mockgps.h"
#include "Mocki2c.h"
#include "compass.c"
#include "project.h"

void setUp(void) {}

void tearDown(void) {}

void test_complete_path_get_checkpoint(void) {
  TEST_IGNORE();
  current_gps_coordinates.BRIDGE_GPS_latitude = 37.334462f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.910785f;

  destination_gps_coordinates.BRIDGE_GPS_latitude = 37.334748f;
  destination_gps_coordinates.BRIDGE_GPS_longitude = -121.910059f;
  gps_navigation_path__find_path();
  printf("new path ...\n");
  destination_gps_coordinates.BRIDGE_GPS_latitude = 0.0f;
  destination_gps_coordinates.BRIDGE_GPS_longitude = 0.0f;
  gps_navigation_path__find_path();
  printf("other\n");
  current_gps_coordinates.BRIDGE_GPS_latitude = 0.0f;
  current_gps_coordinates.BRIDGE_GPS_longitude = 0.0f;
  destination_gps_coordinates.BRIDGE_GPS_latitude = 37.334748f;
  destination_gps_coordinates.BRIDGE_GPS_longitude = -121.910059f;
  gps_navigation_path__find_path();
  dbc_BRIDGE_GPS_s next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.338844, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.880424, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.338844f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.880424f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.338734, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.880646, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.338734f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.880646f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339100f, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.880783f, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.339100f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.880783f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339237, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.880852, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.339237f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.880852f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339432f, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.881027f, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.339432f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.881027f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339531, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.881149, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.339531f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.881149f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339607, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.881317, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.339607f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.881317f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339764f, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.881035f, next_checkpoint.BRIDGE_GPS_longitude);

  current_gps_coordinates.BRIDGE_GPS_latitude = 37.339764f;
  current_gps_coordinates.BRIDGE_GPS_longitude = -121.881035f;
  next_checkpoint = gps_navigation_path__route_finding_main();
  printf("%f:%f\n", next_checkpoint.BRIDGE_GPS_latitude, next_checkpoint.BRIDGE_GPS_longitude);
  TEST_ASSERT_EQUAL_FLOAT(37.339854f, next_checkpoint.BRIDGE_GPS_latitude);
  TEST_ASSERT_EQUAL_FLOAT(-121.881084f, next_checkpoint.BRIDGE_GPS_longitude);
}