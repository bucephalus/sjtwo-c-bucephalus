#include "gps_navigation_path.h"
#include "compass.h"
const int start_node_number = MAX_SIZE - 2;
const int destination_node_number = MAX_SIZE - 1;
float distance_of_checkpoints[MAX_SIZE][MAX_SIZE] = {0};
static int path[MAX_SIZE];
static int path_count = 0;
static int path_current_index = 0;
static const float MAXIMUM_PATH_DISTANCE = 500.0f;
int check_path_checkpoint(int index) { return path[index]; }

void distance_of_checkpoints_clear(void) {
  for (int i = 0; i < MAX_SIZE; i++) {
    for (int j = 0; j < MAX_SIZE; j++) {
      distance_of_checkpoints[i][j] = 0;
    }
  }
}

dbc_BRIDGE_GPS_s checkpoints_array[MAX_SIZE] = {

    {{0}, 37.335995, -121.881840}, // 0
    {{0}, 37.335492, -121.881459}, // 1
    {{0}, 37.335302, -121.881285}, // 2
    {{0}, 37.335189, -121.881566}, // 3
    {{0}, 37.335207, -121.881214}, // 4
    {{0}, 37.334697, -121.880845}, // 5
    {{0}, 37.334558, -121.880932}, // 6
    {{0}, 37.335057, -121.881850}, // 7
                                   // {{0}, 37.335739, -121.881714}, // 3
    {{0}, 0, 0},                   // 8 source
    {{0}, 0, 0},                   // 9 destination

};
void gps_navigation_path__construct_all_routes(void) {

  gps_navigation_path__construct_route(0, 1);
  gps_navigation_path__construct_route(1, 2);
  gps_navigation_path__construct_route(2, 3);
  gps_navigation_path__construct_route(3, 7);
  gps_navigation_path__construct_route(2, 4);
  gps_navigation_path__construct_route(4, 5);
  gps_navigation_path__construct_route(5, 6);
  // gps_navigation_path__construct_route(5, 7);
}

bool gps_navigation_path__find_path(void) {
  bool find_path_flag = false;
  dbc_BRIDGE_GPS_s current_coordinates = compass__get_current_coordinates();
  dbc_BRIDGE_GPS_s destination_coordinates = compass__get_destination_coordinates();

  if (compass__calculate_destination_distance(current_coordinates, destination_coordinates) <= MAXIMUM_PATH_DISTANCE) {
    gps_navigation_path__distance_calculating_matrix();
    gps_navigation_path__init_path_finding();
    gps_navigation_path__implement_dijkstra(distance_of_checkpoints, MAX_SIZE, start_node_number,
                                            destination_node_number);
    find_path_flag = true;
  }
  return find_path_flag;
}

void gps_navigation_path__init_path_finding(void) {
  float start_distance = INFINITY, destination_distance = INFINITY;
  int start_index = -1, destination_index = -1;

  checkpoints_array[MAX_SIZE - 2] = compass__get_current_coordinates();
  checkpoints_array[MAX_SIZE - 1] = compass__get_destination_coordinates();

  for (int i = 0; i < MAX_SIZE - 2; i++) {
    if (compass__calculate_destination_distance(checkpoints_array[MAX_SIZE - 2], checkpoints_array[i]) <
        start_distance) {
      start_distance = compass__calculate_destination_distance(checkpoints_array[MAX_SIZE - 2], checkpoints_array[i]);
      start_index = i;
    }
    if (compass__calculate_destination_distance(checkpoints_array[MAX_SIZE - 1], checkpoints_array[i]) <
        destination_distance) {
      destination_distance =
          compass__calculate_destination_distance(checkpoints_array[MAX_SIZE - 1], checkpoints_array[i]);
      destination_index = i;
    }
  }
  gps_navigation_path__construct_route(MAX_SIZE - 2, start_index);
  gps_navigation_path__construct_route(MAX_SIZE - 1, destination_index);
}

dbc_BRIDGE_GPS_s gps_navigation_path__route_finding_main(void) {

  current_location = compass__get_current_coordinates();

  if (is_checkpoint_arrived()) {
    if (path_current_index != 0)
      path_current_index--;
  }
  gps_next_checkpoint.BRIDGE_GPS_latitude = checkpoints_array[path[path_current_index]].BRIDGE_GPS_latitude;
  gps_next_checkpoint.BRIDGE_GPS_longitude = checkpoints_array[path[path_current_index]].BRIDGE_GPS_longitude;
  return checkpoints_array[path[path_current_index]];
}

bool is_checkpoint_arrived(void) {
  return (compass__calculate_destination_distance(current_location, checkpoints_array[path[path_current_index]]) < 5);
}

void gps_navigation_path__distance_calculating_matrix(void) {
  for (int row = 0; row < MAX_SIZE; row++) {
    for (int col = 0; col < MAX_SIZE; col++) {
      if (row == col) {
        distance_of_checkpoints[row][col] = 0;
      } else {
        distance_of_checkpoints[row][col] = INFINITY;
      }
    }
  }

  gps_navigation_path__construct_all_routes();
}

void gps_navigation_path__construct_route(int row, int col) {
  distance_of_checkpoints[row][col] =
      compass__calculate_destination_distance(checkpoints_array[row], checkpoints_array[col]);
  distance_of_checkpoints[col][row] = distance_of_checkpoints[row][col];
}

void gps_navigation_path__implement_dijkstra(float weight_of_nodes[MAX_SIZE][MAX_SIZE], int size, int start_node,
                                             int end_node) {
  float distance_of_nodes[MAX_SIZE], l_distance = 0.0f;
  int predicted_node[MAX_SIZE], nodes_visited[MAX_SIZE], count, i, j;
  int nextnode = -1;

  for (i = 0; i < size; i++) {
    distance_of_nodes[i] = weight_of_nodes[start_node][i];
    predicted_node[i] = start_node;
    nodes_visited[i] = 0;
  }

  distance_of_nodes[start_node] = 0;
  nodes_visited[start_node] = 1;
  count = 1;
  while (count < size - 1) {
    l_distance = INFINITY;
    for (i = 0; i < size; i++) {
      if (distance_of_nodes[i] < l_distance && !nodes_visited[i]) {
        l_distance = distance_of_nodes[i];
        nextnode = i;
      }
    }

    nodes_visited[nextnode] = 1;
    for (i = 0; i < size; i++)
      if (!nodes_visited[i])
        if (l_distance + weight_of_nodes[nextnode][i] < distance_of_nodes[i]) {
          distance_of_nodes[i] = l_distance + weight_of_nodes[nextnode][i];
          predicted_node[i] = nextnode;
        }
    count++;
  }

  j = end_node;
  path_count = 0;
  path[path_count] = end_node;
  path_count++;
  do {
    j = predicted_node[j];
    path[path_count] = j;
    path_count++;
  } while (j != start_node);

  for (int j = 0; j < path_count; j++) {
    printf("path is %d\n", path[j]);
  }
  path_current_index = path_count - 1;
}
