#pragma once

#include "project.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define INFINITY 9999
#define MAX_SIZE 10

dbc_BRIDGE_GPS_s gps_next_checkpoint;
dbc_BRIDGE_GPS_s current_location;
dbc_BRIDGE_GPS_s destination;

int check_path_checkpoint(int index);

void distance_of_checkpoints_clear(void);

void gps_navigation_path__construct_all_routes(void);

bool gps_navigation_path__find_path(void);

void gps_navigation_path__init_path_finding(void);

dbc_BRIDGE_GPS_s gps_navigation_path__route_finding_main(void);

bool is_checkpoint_arrived(void);

void gps_navigation_path__construct_route(int row, int col);

void gps_navigation_path__implement_dijkstra(float weight_of_nodes[MAX_SIZE][MAX_SIZE], int size, int start_node,
                                             int end_node);

void gps_navigation_path__distance_calculating_matrix(void);