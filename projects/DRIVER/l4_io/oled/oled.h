#pragma once

#include "stdint.h"

void oled__init();
void oled__update();
void DrawPixel(uint32_t x, uint32_t y);