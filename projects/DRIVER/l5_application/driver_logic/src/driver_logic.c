#include "driver_logic.h"
#include "driving_algo.h"
#include "headlights_handler.h"
#include "sjvalley_lcd.h"
#include <stdio.h>

static bool CAR_IN_START_MODE = false;
static dbc_MOTOR_SPEED_s car_speed = {};
static dbc_GEO_DEBUG_s car_current_location = {};

static const dbc_DRIVER_STEER_SPEED_s STOP_COMMAND = {{}, DRIVER_STEER_direction_STRAIGHT, 0};

dbc_DRIVER_STEER_SPEED_s driver_logic__get_motor_command(void) {
  dbc_DRIVER_STEER_SPEED_s current_state;
  static dbc_DRIVER_STEER_SPEED_s last_state = {{0}, 0, 0};
  if (CAR_IN_START_MODE == true) {
    current_state = driving_algo__compute_heading();
    if (current_state.DRIVER_STEER_move_speed == DRIVER_STEER_move_REVERSE_at_SPEED &&
        last_state.DRIVER_STEER_move_speed == DRIVER_STEER_move_FORWARD_at_SPEED) {
      current_state.DRIVER_STEER_move_speed = DRIVER_STEER_move_STOP;
    }
  } else {
    current_state = STOP_COMMAND;
  }
  last_state = current_state;
  return current_state;
}

void driver_logic__set_car_mode(dbc_CAR_ACTION_s car_action) {
  if (car_action.CAR_ACTION_cmd > 0) {
    CAR_IN_START_MODE = true;
  } else {
    CAR_IN_START_MODE = false;
  }
}

void driver_logic__set_car_current_speed(dbc_MOTOR_SPEED_s l_car_speed) { car_speed = l_car_speed; }

void driver_logic__print_on_lcd_current_car_speed(void) {
  char car_speed_info_string[20] = {};
  char car_pwm_info_string[20] = {};
  snprintf(car_speed_info_string, 20, "SPEED=%f", (double)car_speed.MOTOR_SPEED_info);
  // snprintf(car_pwm_info_string, 20, "PWM=%f", (double)car_speed.MOTOR_SPEED_pwm);
  sjvalley_lcd__send_line(1, car_speed_info_string);
  // sjvalley_lcd__send_line(2, car_pwm_info_string);
}

void driver_logic__set_headlight_status(dbc_HEADLIGHT_s headlight) {
  if (headlight.HEADLIGHT_SWITCH) {
    headlights_handler__turn_headlights_on();
  } else {
    headlights_handler__turn_headlights_off();
  }
}

void driver_logic__set_currrent_gps(dbc_GEO_DEBUG_s car_gps_data) {
  car_current_location = car_gps_data;
  char gps_string_lat[20] = {};
  char gps_string_long[20] = {};
  snprintf(gps_string_lat, 20, "%f", car_current_location.GEO_GPS_latitude);
  snprintf(gps_string_long, 20, "%f", car_current_location.GEO_GPS_longitude);
  sjvalley_lcd__send_line(2, gps_string_lat);
  sjvalley_lcd__send_line(3, gps_string_long);
}