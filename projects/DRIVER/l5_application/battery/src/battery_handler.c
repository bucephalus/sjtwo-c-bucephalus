#include "battery_handler.h"
#include "adc.h"
#include "gpio.h"
#include <stdio.h>

static gpio_s battery;
static int battery_charge_percentage = 0;

static float max_voltage_of_single_cell = 4.2;
static float battery_voltages[21] = {4.2,  4.15, 4.11, 4.08, 4.02, 3.98, 3.95, 3.91, 3.87, 3.85, 3.84,
                                     3.82, 3.8,  3.79, 3.77, 3.75, 3.73, 3.71, 3.69, 3.61, 3.27};
static float battery_percentage_of_max_voltage[21];
static int battery_charge_capacity[21];

static void battery_handler__fill_battery_charge_capacity_table() {
  for (int i = 0; i < 21; i++) {
    battery_charge_capacity[i] = (20 - i) * 5;
  }
}

static void battery_handler__fill_battery_percentage_of_max_voltage_table() {
  for (int i = 0; i < 21; i++) {
    battery_percentage_of_max_voltage[i] = (battery_voltages[i] / max_voltage_of_single_cell) * 100;
  }
}

static uint16_t battery_handler__get_raw_ADC_value(void) {
  uint16_t raw_value = 0;
  raw_value = adc__get_adc_value(ADC__CHANNEL_4);
  return raw_value;
}

void battery_handler__init(void) {
  battery = gpio__construct_with_function(GPIO__PORT_1, 30, GPIO__FUNCTION_1); // ADC Channel 4
  adc__initialize();
  battery_handler__fill_battery_charge_capacity_table();
  battery_handler__fill_battery_percentage_of_max_voltage_table();
}

void battery_handler__compute_battery_charge_percentage(void) {
  int adc_val = 0;
  float percentage = 0;

  adc_val = battery_handler__get_raw_ADC_value();
  percentage = (adc_val / 40.95); //(adc_val/4095)*100

  for (int i = 0; i < 21; i++) {
    if (percentage <= battery_percentage_of_max_voltage[20]) {
      battery_charge_percentage = 0;
      break;
    } else if ((percentage <= battery_percentage_of_max_voltage[i]) &&
               (percentage > battery_percentage_of_max_voltage[i + 1])) {
      battery_charge_percentage = battery_charge_capacity[i];
      break;
    }
  }
}

int battery_handler__get_battery_charge_percentage(void) { return battery_charge_percentage; }