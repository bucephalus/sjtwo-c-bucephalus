#pragma once

void battery_handler__init(void);
void battery_handler__compute_battery_charge_percentage(void);
int battery_handler__get_battery_charge_percentage(void);
