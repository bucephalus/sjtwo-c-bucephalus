#include "bridge_controller_handler.h"
#include "lpc40xx.h"
#include "lpc_peripherals.h"
#include <string.h>

static int address_count = 0;
static bool car_action_status = false;
static bool car_headlight_status = false;
static bool car_test_status = false;
static bool ping_status = false;
float debug_motor_speed = 0.0f;
float debug_motor_speed_pwm = 0.0f;
float latitude = 0;
float longitude = 0;
float debug_geo_compass_current_heading;
float debug_geo_compass_destination_heading;
float debug_geo_compass_distance;
int debug_steer_move_speed;
int debug_battery_percentage = 0;
DRIVER_STEER_direction_e debug_steer_direction;

void bridge_controller_handler__initialize_bluetooth_module(void) {

  gpio__construct_with_function(GPIO__PORT_4, 28, GPIO__FUNCTION_2); // Board_Tx
  gpio__construct_with_function(GPIO__PORT_4, 29, GPIO__FUNCTION_2); // Board_Rx

  QueueHandle_t rxq_handle = xQueueCreate(200, sizeof(unsigned char));
  QueueHandle_t txq_handle = xQueueCreate(200, sizeof(unsigned char));

  // UART  Initialize
  uart__init(UART__3, clock__get_peripheral_clock_hz(), 9600U); // original: 38400U
  uart__enable_queues(UART__3, rxq_handle, txq_handle);
}

void bridge_controller_handler__parse_gps_data(const char *input_buffer) {
  sscanf(input_buffer, "GPS%f,%f", &latitude, &longitude);
}

void bridge_controller_handler__get_data_from_uart(void) {
  unsigned char byte;
  while (uart__get(UART__3, &byte, 0)) {
    push_buffer(byte);
  }
}

void bridge_controller_handler__get_single_gps_message(char *line) {
  int i = 0;
  int substring_start_index = sl_string__first_index_of(gps_buffer, "GPS");
  int index = substring_start_index;

  while (gps_buffer[index] != '#') {
    line[i] = gps_buffer[index];
    ++index;
    ++i;
  }
  line[i] = '\0';

  sl_string__erase_after(gps_buffer, substring_start_index, i + 3);
  rear = rear - (i + 3);
}

bool bridge_controller_handler__get_gps_message_from_buffer(char *temp_buffer) {
  bool return_value = false;

  if (sl_string__contains(gps_buffer, "GPS")) {
    bridge_controller_handler__get_single_gps_message(temp_buffer);
    return_value = true;
  }
  return return_value;
}

void bridge_controller_handler__get_gps_coordinates() {
  char temp_buffer[100];
  if (bridge_controller_handler__get_gps_message_from_buffer(temp_buffer)) {
    bridge_controller_handler__parse_gps_data(temp_buffer);
  }
}

void bridge_controller_handler__get_destination_coordinates() {

  bridge_controller_handler__get_data_from_uart();
  bridge_controller_handler__get_gps_coordinates();
}

bool bridge_controller_handler__buffer_has_message(void) { return (!is_buffer_empty()); }

bool bridge_controller_handler__get_start_stop_condition(void) {
  // bridge_controller_handler__get_data_from_uart();

  if (bridge_controller_handler__buffer_has_message()) {

    if (sl_string__contains(gps_buffer, "START")) {
      car_action_status = true;
      // puts("CA-Start\n");
      int index_start = sl_string__first_index_of(gps_buffer, "START");
      sl_string__erase_after(gps_buffer, index_start, 7);
      rear = rear - 7;

    } else if (sl_string__contains(gps_buffer, "STOP")) {
      car_action_status = false;
      // puts("CA-Stop\n");
      int index_stop = sl_string__first_index_of(gps_buffer, "STOP");
      sl_string__erase_after(gps_buffer, index_stop, 6);
      rear = rear - 6;
    }
  }
  // printf("Buffer Index: %d\n", rear);
  return car_action_status;
}

bool bridge_controller_handler__get_headlight_control_signal(void) {
  if (bridge_controller_handler__buffer_has_message()) {

    if (sl_string__contains(gps_buffer, "HON")) {
      car_headlight_status = true;
      // puts("H-ON\n");
      int index_hon = sl_string__first_index_of(gps_buffer, "HON");
      sl_string__erase_after(gps_buffer, index_hon, 5);
      rear = rear - 5;

    } else if (sl_string__contains(gps_buffer, "HOFF")) {
      car_headlight_status = false;
      // puts("H-OFF\n");
      int index_hoff = sl_string__first_index_of(gps_buffer, "HOFF");
      sl_string__erase_after(gps_buffer, index_hoff, 6);
      rear = rear - 6;
    }
  }
  return car_headlight_status;
}

bool bridge_controller_handler__get_test_button_control_signal(void) {

  if (bridge_controller_handler__buffer_has_message()) {

    if (sl_string__contains(gps_buffer, "MTEST")) {
      car_test_status = true;
      // puts("MTEST\n");
      int index_test = sl_string__first_index_of(gps_buffer, "MTEST");
      sl_string__erase_after(gps_buffer, index_test, 7);
      rear = rear - 7;

    } else {
      car_test_status = false;
    }
  }
  return car_test_status;
}

void bridge_controller_handler__send_ping_status(void) {
  if (sl_string__contains(gps_buffer, "PING")) {
    ping_status = true;
    int index_ping = sl_string__first_index_of(gps_buffer, "PING");
    sl_string__erase_after(gps_buffer, index_ping, 6);
    rear = rear - 6;

  } else {
    ping_status = false;
  }
}

void bridge_controller_handler__send_debug_info(void) {

  /*
  Debug Information Data Format -
  Ping Status
  Car Action
  Motor Speed
  Motor Speed PWM
  Ultrasonic left
  Ultrasonic right
  Ultrasonic back
  Ultrasonic front
  Geo compass current heading
  Geo compass destination heading
  Geo compass compass distance
  Driver Steer Speed
  Driver Steer Direction
  Headlight
  Test Button
  GPS Latitude
  GPS Longitude
  Battery Percentage
  */

  char output_string[100];
  char ping_status_string[10];
  int i = 0;
  sensor_t sensor_values;
  ultrasonic_sensor_handler__get_all_sensor_values(&sensor_values);

  if (ping_status)
    strcpy(ping_status_string, "ACK");
  else
    strcpy(ping_status_string, "NULL");

  snprintf(output_string, 100, "*%s,%d,%f,%f,%d,%d,%d,%d,%.2f,%.2f,%.2f,%d,%d,%d,%d,%f,%f,%i#\n", ping_status_string,
           car_action_status, debug_motor_speed, debug_motor_speed_pwm, sensor_values.left, sensor_values.right,
           sensor_values.back, sensor_values.front, debug_geo_compass_current_heading,
           debug_geo_compass_destination_heading, debug_geo_compass_distance, debug_steer_move_speed,
           debug_steer_direction, car_headlight_status, car_test_status, latitude, longitude, debug_battery_percentage);
  while (output_string[i] != '\0') {
    uart__put(UART__3, output_string[i], 0);
    ++i;
  }
}
