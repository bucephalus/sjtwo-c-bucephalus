#include "unity.h"

#include "bridge_buffer.h"

void test_is_buffer_empty_negative() {
  front = -1;
  rear = -1;
  TEST_ASSERT_TRUE(is_buffer_empty());
}

void test_is_buffer_empty_positive() {
  front = -1;
  rear = -1;

  push_buffer('A');
  push_buffer('B');
  TEST_ASSERT_FALSE(is_buffer_empty());
}

void test_push_buffer_negative() {
  TEST_IGNORE();
  front = -1;
  rear = -1;
  gps_buffer[SIZE];
  memset(gps_buffer, 0, SIZE);
  char byte = 160;

  push_buffer(&byte);
  push_buffer(&byte);
  push_buffer(&byte);
  push_buffer(&byte);
  push_buffer(&byte);

  TEST_ASSERT_EQUAL(0, gps_buffer[0]);
}

void test_pop_buffer_two() {

  char byte;
  front = -1;
  rear = -1;

  TEST_ASSERT_FALSE(pop_buffer(&byte));
}
