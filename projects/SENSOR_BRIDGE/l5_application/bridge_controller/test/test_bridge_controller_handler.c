#include "unity.h"

#include <string.h>

#include "Mockgpio.h"
#include "sl_string.h"

#include "Mockclock.h"
#include "Mockuart.h"

#include "Mockqueue.h"
#include "Mockultrasonic_sensor_handler.h"

#include "bridge_buffer.h"
#include "bridge_controller_handler.h"

// void test_bridge_controller_handler__initialize_bluetooth_module() {

//   gpio_s gpio_data;
//   clock__get_peripheral_clock_hz_ExpectAndReturn(38400U);
//   uart__init_Expect(UART__3, 38400, 9600);
//   QueueHandle_t rxq_handle;
//   QueueHandle_t txq_handle;
//   xQueueCreate_ExpectAndReturn(200, sizeof(unsigned char), rxq_handle);
//   xQueueCreate_ExpectAndReturn(200, sizeof(unsigned char), txq_handle);
//   uart__enable_queues_ExpectAndReturn(UART__3, rxq_handle, txq_handle, true);

//   gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 28, GPIO__FUNCTION_2, gpio_data);
//   gpio__construct_with_function_ExpectAndReturn(GPIO__PORT_4, 29, GPIO__FUNCTION_2, gpio_data);

//   bridge_controller_handler__initialize_bluetooth_module();
// }

void test_bridge_controller_handler__buffer_has_message() {
  front = 2;
  rear = 1;
  TEST_ASSERT_TRUE(bridge_controller_handler__buffer_has_message());
}

void test_bridge_controller_handler__parse_gps_data() {
  latitude = 0;
  longitude = 0;
  bridge_controller_handler__parse_gps_data("GPS12.345,-54.789");
  TEST_ASSERT_EQUAL(latitude, 12.345);
  TEST_ASSERT_EQUAL(longitude, -54.789);
}

void test_bridge_controller_handler__get_start_stop_condition() {
  front = 0;
  rear = 12;
  strcpy(gps_buffer, "STARTxyzsdfg");
  TEST_ASSERT_TRUE(bridge_controller_handler__get_start_stop_condition());
}

void test_bridge_controller_handler__get_start_stop_condition_2() {
  front = 0;
  rear = 11;
  strcpy(gps_buffer, "STOPxyzsdfg");
  TEST_ASSERT_FALSE(bridge_controller_handler__get_start_stop_condition());
}

void test_bridge_controller_handler__get_headlight_control_signal() {
  front = 0;
  rear = 7;
  strcpy(gps_buffer, "STOPHON");
  TEST_ASSERT_TRUE(bridge_controller_handler__get_headlight_control_signal());
}

void test_bridge_controller_handler__get_headlight_control_signal_2() {
  front = 0;
  rear = 8;
  strcpy(gps_buffer, "STOPHOFF");
  TEST_ASSERT_FALSE(bridge_controller_handler__get_headlight_control_signal());
}

void test_bridge_controller_handler__get_test_button_control_signal() {
  front = 0;
  rear = 20;
  strcpy(gps_buffer, "MTESTioio");
  TEST_ASSERT_TRUE(bridge_controller_handler__get_test_button_control_signal());
}

void test_bridge_controller_handler__get_test_button_control_signal_2() {
  front = 0;
  rear = 20;
  strcpy(gps_buffer, "Mhsaafa");
  TEST_ASSERT_FALSE(bridge_controller_handler__get_test_button_control_signal());
}
