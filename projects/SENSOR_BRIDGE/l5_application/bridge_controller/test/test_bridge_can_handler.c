#include "unity.h"

#include "Mockbridge_controller_handler.h"
#include "Mockcan_bus.h"
#include "project.h"

#include "bridge_can_handler.h"

float debug_motor_speed = 0.0f;
float debug_motor_speed_pwm = 0.0f;
float debug_geo_compass_current_heading;
float debug_geo_compass_destination_heading;
float debug_geo_compass_distance;
float latitude, longitude;
int debug_steer_move_speed;
int debug_battery_percentage = 0;

DRIVER_STEER_direction_e debug_steer_direction;

void test_bridge_can_handler__transmit_messages_10hz(void) {
  dbc_BRIDGE_GPS_s bridge_struct = {};

  bridge_controller_handler__get_destination_coordinates_Expect();
  bridge_struct.BRIDGE_GPS_latitude = latitude;
  bridge_struct.BRIDGE_GPS_longitude = longitude;

  can__msg_t bridge_can_msg = {};
  const dbc_message_header_t bridge_header = dbc_encode_BRIDGE_GPS(bridge_can_msg.data.bytes, &bridge_struct);

  bridge_can_msg.msg_id = bridge_header.message_id;
  bridge_can_msg.frame_fields.data_len = bridge_header.message_dlc;

  can__tx_ExpectAnyArgsAndReturn(true);

  bridge_can_handler__transmit_messages_10hz();
}

void test_bridge_can_handler__transmit_start_stop_condition(void) {

  dbc_CAR_ACTION_s car_action_struct;
  can__msg_t car_action_can_msg = {};

  bridge_controller_handler__get_start_stop_condition_ExpectAndReturn(true);

  const dbc_message_header_t car_action_header =
      dbc_encode_CAR_ACTION(car_action_can_msg.data.bytes, &car_action_struct);

  car_action_can_msg.msg_id = car_action_header.message_id;
  car_action_can_msg.frame_fields.data_len = car_action_header.message_dlc;

  can__tx_ExpectAnyArgsAndReturn(true);
  bridge_can_handler__transmit_start_stop_condition();
}

void test_bridge_can_handler__transmit_headlight_control_signal(void) {
  dbc_HEADLIGHT_s headlight_control_signal;

  bridge_controller_handler__get_headlight_control_signal_ExpectAndReturn(true);
  can__msg_t headlight_can_msg = {};

  const dbc_message_header_t headlight_header =
      dbc_encode_HEADLIGHT(headlight_can_msg.data.bytes, &headlight_control_signal);

  headlight_can_msg.msg_id = headlight_header.message_id;
  headlight_can_msg.frame_fields.data_len = headlight_header.message_dlc;

  can__tx_ExpectAnyArgsAndReturn(true);
  bridge_can_handler__transmit_headlight_control_signal();
}

void test_bridge_can_handler__transmit_test_button_control_signal(void) {
  dbc_TEST_BUTTON_s test_button_control_signal;

  bridge_controller_handler__get_test_button_control_signal_ExpectAndReturn(true);
  can__msg_t test_button_can_msg = {};

  const dbc_message_header_t test_button_header =
      dbc_encode_TEST_BUTTON(test_button_can_msg.data.bytes, &test_button_control_signal);

  test_button_can_msg.msg_id = test_button_header.message_id;
  test_button_can_msg.frame_fields.data_len = test_button_header.message_dlc;

  can__tx_ExpectAnyArgsAndReturn(true);
  bridge_can_handler__transmit_test_button_control_signal();
}

void test_bridge_can_handler__handle_all_incoming_messages(void) {
  dbc_MOTOR_SPEED_s motor_speed_message = {};
  dbc_GEO_COMPASS_s geo_compass_message = {};
  dbc_DRIVER_STEER_SPEED_s driver_steer_message = {};
  can__msg_t can_msg = {};

  can__rx_ExpectAnyArgsAndReturn(true);
  const dbc_message_header_t header = {
      .message_id = can_msg.msg_id,
      .message_dlc = can_msg.frame_fields.data_len,
  };

  dbc_decode_MOTOR_SPEED(&motor_speed_message, header, can_msg.data.bytes);
  dbc_decode_GEO_COMPASS(&geo_compass_message, header, can_msg.data.bytes);
  dbc_decode_DRIVER_STEER_SPEED(&driver_steer_message, header, can_msg.data.bytes);
  can__rx_ExpectAnyArgsAndReturn(false);
  bridge_can_handler__handle_all_incoming_messages();
}