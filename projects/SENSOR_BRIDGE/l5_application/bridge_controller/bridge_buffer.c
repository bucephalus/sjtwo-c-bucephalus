#include "bridge_buffer.h"

int front = -1, rear = -1;
int buffer_index = 0;
unsigned char gps_buffer[SIZE];

bool is_buffer_empty() {
  if (rear == -1)
    return true;
  else
    return false;
}

void push_buffer(char value) {

  if (isalnum(value) || isspace(value) || ispunct(value)) {
    if (rear == -1) {
      rear = 0;
    } else {
      rear = (rear + 1) % SIZE;
    }
    gps_buffer[rear] = value;
  }
}

void clear_buffer(void) { front = rear = -1; }

bool pop_buffer(char *value) {

  if (is_buffer_empty()) {
    return false;
  } else {
    if (front == rear) {
      *value = gps_buffer[front];
      front = -1;
      rear = -1;
    } else {
      *value = gps_buffer[front];
      front = (front + 1) % SIZE;
    }

    return true;
  }
}

int get_count(void) {
  int count = (rear + SIZE - front) % SIZE + 1;
  return count;
}

void print_buffer(void) {
  int count = get_count();
  for (int i = 0; i < count; i++) {
    int index = (front + i) % SIZE;
    // printf("%c", gps_buffer[index]);
  }
}
