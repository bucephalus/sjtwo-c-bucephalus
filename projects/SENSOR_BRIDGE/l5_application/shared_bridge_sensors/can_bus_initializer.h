#pragma once

void can_bus_initializer__initialize_can1(void);

void can_bus_initializer__reset_if_bus_off_can1(void);

void can_bus_initializer__initialize_can2(void);

void can_bus_initializer__reset_if_bus_off_can2(void);