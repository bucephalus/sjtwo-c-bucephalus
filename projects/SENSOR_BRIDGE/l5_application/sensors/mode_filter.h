#pragma once

#include <stdint.h>

uint16_t mode_filter__find_mode_of_sensor_values_converted_to_cm(uint16_t *sensor_values, int numb_of_sensor_values);