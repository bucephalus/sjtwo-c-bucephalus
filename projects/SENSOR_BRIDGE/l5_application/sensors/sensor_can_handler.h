#pragma once

// for testing purposes
void sensor_can_handler__transmit_messages_1hz(void);
void sensor_can_handler__transmit_messages_10hz(void);

void sensor_can_handler__transmit_messages_50hz(void);

// for testing purposes
void sensor_can_handler__handle_all_incoming_messages(void);